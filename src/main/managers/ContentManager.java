package main.managers;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ContentManager {

    public static Boolean contentValidator(List<String> content) {
        Boolean isValid = true;

        content.removeAll(Arrays.asList(""));
        String[] contentArray = content.toArray(new String[0]);

        if (contentArray.length == 0) {
            return false;
        }

        for (int line = 0; line < contentArray.length; line++) {
            Matcher matcher = Pattern.compile("^(\\d+;[a-zA-Z].*?;[0-9]{2}\\/[0-9]{2}\\/[0-9]{4}+;\\d+.*?;)")
                    .matcher(contentArray[line]);

            if (!matcher.find() || contentArray[line].split(";").length != 4) {
                isValid = false;
                break;
            }
        }

        return isValid;
    }

}
