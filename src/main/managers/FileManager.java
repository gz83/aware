package main.managers;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileManager {

    public static void createFolder(Path path) throws Exception {
        if (!Files.exists(path)) {
            Files.createDirectories(path);
        }
    }

    public static List<Path> findFiles(Path path, String extension) throws Exception {
        List<Path> files;

        try (Stream<Path> walk = Files.walk(path)) {
            files = walk
                    .filter(Files::isRegularFile)
                    .filter(file -> file.getFileName().toString().endsWith(extension))
                    .collect(Collectors.toList());
        }

        return files;
    }

    public static List<String> readFile(Path path) throws Exception {
        return Files.readAllLines(path, StandardCharsets.UTF_8);
    }

    public static void moveFile(Path filePath, Path directory) throws Exception {
        Files.move(filePath, directory.resolve(filePath.getFileName()), StandardCopyOption.REPLACE_EXISTING);
    }

}
