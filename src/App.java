import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import main.managers.ContentManager;
import main.managers.FileManager;

public class App {

    private static String pendingFolder = "/PENDENTES";
    private static String validatedFolder = "/VALIDADOS";
    private static String invalidatedFolder = "/INVALIDADOS";

    public static void main(String[] args) throws Exception {
        Path validatedPath = directoryPath(validatedFolder);
        Path invalidatedPath = directoryPath(invalidatedFolder);

        FileManager.createFolder(validatedPath);
        FileManager.createFolder(invalidatedPath);

        Path pendingPath = directoryPath(pendingFolder);
        List<Path> files = FileManager.findFiles(pendingPath, ".csv");

        for (Path path : files) {
            List<String> fileContent = FileManager.readFile(path);
            Boolean isValid = ContentManager.contentValidator(fileContent);

            FileManager.moveFile(path, directoryPath(isValid ? validatedFolder : invalidatedFolder));
        }
    }

    private static Path directoryPath(String folder) {
        return Paths.get(System.getProperty("user.dir") + folder);
    }

}
